#ifndef LEARNING_CRCFACTORY_H
#define LEARNING_CRCFACTORY_H

#include <cstddef>
#include <cstdint>

#include "crc.h"

using namespace std;

class CrcFactory {
public:
    static Crc* makeCrcFromData(uint8_t* pData, size_t dataLength);
};


#endif //LEARNING_CRCFACTORY_H
