#ifndef LEARNING_CRC16_H
#define LEARNING_CRC16_H

#include <cstddef>
#include <cstdint>

#include "crc.h"

/*
  Name  : CRC-16 CCITT
  Poly  : 0x1021    x^16 + x^12 + x^5 + 1
  Init  : 0xFFFF
  Revert: false
  XorOut: 0x0000
  Check : 0x29B1 ("123456789")
  MaxLen: 4095 байт (32767 бит) - обнаружение
    одинарных, двойных, тройных и всех нечетных ошибок
*/

class Crc16 : public Crc {
public:
    Crc16(const uint8_t *pData, const size_t dataLength);
    ~Crc16();

    void calcCrc();
};


#endif //LEARNING_CRC16_H
