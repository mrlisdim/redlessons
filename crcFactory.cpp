#include <cstddef>
#include <cstdint>

#include "crcFactory.h"

#include "crc8.h"
#include "crc16.h"

Crc* CrcFactory::makeCrcFromData(uint8_t* pData, size_t dataLength) {
    if (pData) {
        switch (pData[0]) {
            case 0:
                return new Crc8(pData, dataLength);
            break;
            case 1:
                return new Crc16(pData, dataLength);
            break;
            default:
                return nullptr;
        }
    }
    return nullptr;
}
