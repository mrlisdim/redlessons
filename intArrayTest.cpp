#include <iostream>
#include <cstdint>


#include "intArray.h"

using namespace std;

void printAll(const IntArray& array) {
    /* IntArray::getSize() */
    for (int32_t i = 0; i < array.getSize(); ++i) {
        /* IntArray::get() */
        cout << "a[" << i << "] = " << array.at(i) << endl;
    }
}

int main() {
    cout << "Hello, World!\n" << endl;

    /* Constructor IntArray(int) */
    cout << "New array1(10)" << endl;
    IntArray intArray1(10);
    /* IntArray::getSize() + IntArray::get() */
    printAll(intArray1);

    /* Constructor IntArray(int, int) */
    cout << "\nNew array2(10, 10)" << endl;
    IntArray intArray2(10, 10);
    printAll(intArray2);

    /* IntArray::set() */
    cout << "Change array2(10, 10) to i^2" << endl;
    for (int32_t i = 0; i < intArray2.getSize(); ++i) {
        intArray2.set(i, i*i);
    }
    printAll(intArray2);

    cout << "\nNew array3(10, 10)" << endl;
    IntArray* intArray3 = new IntArray(10, 10);
    cout << "New array4(10, 10)" << endl;
    IntArray* intArray4 = new IntArray(10, 10);

    /* IntArray::getCount() */
    cout << "\nCount of all elements of arrays: " << IntArray::getCount() << endl;
    cout << "Delete array4..." << endl;
    delete intArray4;

    cout << "Count of all elements of arrays: " << IntArray::getCount() << endl;

    /* Constructor IntArray(const IntArray&) */
    cout << "Copy array2 into array5" << endl;
    IntArray intArray5(intArray2);
    cout << "Print array5:" << endl;
    printAll(intArray5);

    cout << "\nChange array5 to 0..0" << endl;
    for (int i = 0; i < intArray5.getSize(); ++i) {
        intArray5.set(i, 0);
    }
    cout << "Print array2:" << endl;
    printAll(intArray2);
    cout << "Print array5:" << endl;
    printAll(intArray5);

    cout << endl;
    cout << intArray2[2] << endl;
    intArray2[2] = 123;
    cout << intArray2[2] << endl;
    int a = intArray2[2];
    cout << intArray2[2] << endl;

    cout << endl;
    intArray1 = intArray2;
    intArray1[1] = 666;
    printAll(intArray1);

    cout << endl;
    IntArray intArray6;
    intArray6 = intArray5 + intArray2;
    printAll(intArray6);

    cout << endl;
    intArray5.pushBack(333);
    printAll(intArray5);
    cout << IntArray::getCount() << endl;

    cout << endl;
    intArray6.pushBack(intArray5);
    printAll(intArray6);
    cout << IntArray::getCount() << endl;

    return 0;
}