/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <mlisdim@ya.ru> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Lisin D.A.
 * ----------------------------------------------------------------------------
 */

#pragma once

#include <cstdlib>
#include <cstdint>
#include <c++/iosfwd>

using namespace std;

//!  An IntArray class.
/*!

*/
/**
 * @brief An IntArray class
 *
 * Class described array of int
 */
class IntArray
{
	public:

    /**
     * @brief A constructor.
     *
     * Initialize m_data by null.
     */
    IntArray();

    /**
     * @brief A constructor with size.
     *
     * Try to allocate memory enough for
     * size * sizeof(int32_t). Init all
     * array members by zero.
     *
     * @size a count of array members
     */
    IntArray(uint32_t size);

    /**
     * @brief A constructor with size
     *        and init value.
     *
     * Try to allocate memory enough for
     * size * sizeof(int32_t). Init all
     * array members by initValue.
     *
     * @param size a count of array members
     * @param initValue an initialization value
     */
    IntArray(uint32_t size, int32_t initValue);

    /**
     * @brief A coping lvalue constructor.
     *
     * Copy all fields of intArray
     *
     * @param intArray a source array
     */
    IntArray(const IntArray& intArray);

    /**
     * @brief A coping rvalue constructor.
     *
     * Move all fields of source
     *
     * @param source a source array
     */
    IntArray(IntArray&& source);

    /**
     * @brief A destructor.
     *
     * Remove allocate memory,
     * decrease m_count
     *
     * @sa m_count
     */
    ~IntArray();

    /**
     * @brief A method to init array.
     *
     * Allocate memory enough for
     * size * sizeof(int32_t). Init all
     * members of data array by initValue
     *
     * @param size a count of array members
     * @param an initialization value
     * @sa m_count, m_allocMem()
     */
    void init(uint32_t size, int32_t initValue = 0);

    /**
     * @brief A method to clone array.
     *
     * Clone source array. Free data array
     * and try to allocate new for considering
     * source obj array size. Coping all members
     * from source obj.
     *
     * @param sourceObj a source array
     */
    void clone(const IntArray& sourceObj);

    /**
     * @brief A method to get member.
     *
     * Return reference on i-member of array.
     * Check number to out of range.
     *
     * @param i a number of member
     */
    int32_t& at(int32_t i) const;

    /**
     * @brief A method to get size of array.
     *
     * Return count of members.
     */
    uint32_t getSize() const;

    /**
     * @brief A method to set member value.
     *
     * If number not out of bound
     * set n-member value.
     *
     * @param n a member number
     * @param value a value
     */
    void set(int32_t n, int32_t value);

    /**
     * @brief A method to push back member.
     *
     * Push back new member of array with
     * some value. Try to increase memory
     * before pushing if it needed.
     *
     * @param value a value
     * @sa m_tryIncMem()
     */
    void pushBack(int32_t value);

    /**
     * @brief A method to push IntArray obj.
     *
     * Push back all members of source
     * array. Try to increase memory
     * before pushing if it's needed.
     *
     * @param addObj a source array
     * @sa m_tryIncMem()
     */
    void pushBack(const IntArray& addObj);

    /**
     * @brief A method to pop last member.
     *
     * Return last member value
     * and delete it. It's try to decrease
     * memory, if it's needed.
     *
     * @sa m_tryDecMem()
     */
    int32_t popBack();

    int32_t& operator[](int32_t i);
    IntArray& operator= (const IntArray& scrObj);
    IntArray& operator+= (const IntArray& srcObj);

    static uint32_t getCount();

    friend IntArray operator+ (const IntArray& array1, const IntArray& array2);
    friend std::ostream& operator<< (std::ostream &stream, const IntArray& array);

	private:
    /**
     * \brief Allocate memory
     *
     * Method allocate memory considering on count
     * of elements in array (m_size). It use power
     * of two as memory size in bytes.
     *
     * \sa m_size
     */
    void m_allocMem();

    /**
     * \brief Increase memory
     *
     * An inline method try to increase memory if it's
     * needed and it's posible. It's use m_resizeMem()
     * to resize memory.
     *
     * \sa m_tryDecMem(), m_resizeMem()
     */
    void m_tryIncMem();

    /**
     * @brief Decrease memory
     *
     * An inline method to try to decrease memory if it's
     * needed and it's posible. It's use m_resizeMem()
     * to resize memory.
     *
     * @sa m_tryIncMem(), m_resizeMem()
     */
    void m_tryDecMem();

    /**
     * @brief Resize memory
     *
     * A method to resize memory of data array
     * to required size.
     *
     * @param memSize required memory size
     * @sa m_tryIncMem(), m_tryDecMem(), m_allocMem(), m_memSize
     */
    void m_resizeMem(size_t memSize);

    int32_t* m_array;   /**< Pointer on data array. */
    uint32_t m_size;    /**< Count of members of data array. */
    uint32_t m_memSize; /**< Memory size of data array. */

    static uint32_t m_count; /**< Count of all members data array of impl objects. */
    static const uint32_t M_MEM_BLOCK_SIZE; /**< Start memory size. */
};
