#ifndef LEARNING_CRC8_H
#define LEARNING_CRC8_H

#include <cstddef>
#include <cstdint>

#include "crc.h"

/*
  Name  : CRC-8
  Poly  : 0x31    x^8 + x^5 + x^4 + 1
  Init  : 0xFF
  Revert: false
  XorOut: 0x00
  Check : 0xF7 ("123456789")
  MaxLen: 15 байт(127 бит) - обнаружение
    одинарных, двойных, тройных и всех нечетных ошибок
*/

class Crc8 : public Crc {
public:
    Crc8(const uint8_t* pData, const size_t dataLength);
    ~Crc8();

    void calcCrc();
};


#endif //LEARNING_CRC8_H
