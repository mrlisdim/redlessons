#include <cstddef>
#include <cstdint>

#include "crc16.h"

using namespace std;

Crc16::Crc16(const uint8_t* pData, const size_t dataLength) :
    Crc(pData, dataLength)
{ }

Crc16::~Crc16() { }

void Crc16::calcCrc()
{
    uint16_t crc = 0xFFFF;
    uint32_t j;

    for (size_t i = 0; i < m_dataLength; ++i) {
        crc ^= m_pData[i] << 8;

        for (j = 0; j < 8; ++j)
            crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
    }

    m_crc = crc;
}
