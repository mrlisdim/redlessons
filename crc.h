#ifndef LEARNING_CRC_H
#define LEARNING_CRC_H

#include <cstddef>
#include <cstdint>

using namespace std;

class Crc {
public:
    Crc(const uint8_t* pData, const size_t dataLength);
    virtual ~Crc();

    virtual void calcCrc() = 0;
    unsigned int getCrc() {
        return m_crc;
    }

    bool operator== (const Crc& lhs) {
        return lhs.m_crc == m_crc;
    }

protected:
    uint_least32_t m_crc;
    const size_t m_dataLength;
    const uint8_t* m_pData;
};

#endif //LEARNING_CRC_H
