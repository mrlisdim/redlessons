#ifndef DEBUG_H
#define DEBUG_H

#define DEBUG
#if defined(DEBUG)

    #include <iostream>

    #define fSay(x) \
                cout << __TIME__ \
                << ": Func [" << __PRETTY_FUNCTION__ << \
                "] say: " << x << endl

    #define debug(x) \
                cout << "Line " << __LINE__ << " File: " << __FILE__ << endl; \
                fSay(x)
#else
    #define debug(x){}
    #define fSay(x){}

#endif

#endif //DEBUG_H
